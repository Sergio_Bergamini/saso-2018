# Effective Collective Summarisation of Distributed Data in Mobile Environments

Submitted to [SASO 2018 - 12th IEEE International Conference on Self-Adaptive and Self-Organizing Systems](https://saso2018.fbk.eu/), extended version of [this abstract](https://doi.org/10.4204/EPTCS.264.3). For any issues with reproducing the experiments, please contact [Giorgio Audrito](mailto:giorgio.audrito@unito.it).


## Description

This repository contains all the source code used to perform the experiments presented in the paper.


## Prerequisites

* Importing the repository
    - Git

* Executing the simulations
    - Java 8 JDK installed and set as default virtual machine
    - Eclipse Oxygen or above (older versions should work as well), if you want to inspect the code and execute the simulations in a GUI.

* Chrunching the generated data and producing the charts
    - Python 2.7.x installed and set as default Python interpreter
    - [Asymptote 2.44](http://asymptote.sourceforge.net/) vector graphics compiler installed
  
**Notes about the supported operating systems**

All the tests have been performed under Linux, no test has been performed under any other operating system, and we only support reproducing the tests under that environment.
Due to the portable nature of the used tools, the test suite should run also on other Unix-like OSs (e.g. BSD, MacOS X).


## Reproducing the experiment


### Importing the repository

The first step is cloning this repository. It can be easily done by using `git`. Open a terminal, move it into an empty folder of your choice, then issue the following command:

``git clone https://gaudrito@bitbucket.org/Sergio_Bergamini/saso-2018.git``

This should make all the required files appear in your repository, with the latest version.


### Executing the simulations

All the graphs for all simulations can be produced automatically by the command:

``./batch_tester.sh all``

issued from the root directory where the repository was cloned. The script is executed in background and saves output in the `data` folder as text files containing asymptote code, together with their compiled PDF versions.
In case of problems with the fully-automated procedure, or in case you want to vary some parameters, it is also possible to produce them step by step in the following way.

* First, compiled java class files (from `src/main/java/`) and a copy of all files in directory `src/main/protelis/` need to be present in directory `bin/`. If you already run graphical simulations with Eclipse (see the next section), they should already be present. Otherwise, you can produce them with:  
  ``./batch_tester.sh compile``
  
* Then, you can produce the raw data associated to the experiments with the following commands:  
  ``./batch_tester.sh run isolation 10 500 random variability gradient``  
  ``./batch_tester.sh run study 100 7200 random pFollows collection``  
  During execution, progress can be tracked through `log` files named as the experiment names. Raw results will be written in directory `data/raw/`.

* Finally, plots can be extracted with the following commands:  
  ``./batch_tester.sh plot isolation "gradient:variability(sum)+variability=0&time(sum)+variability=0.5&time(sum)+variability=1&time(sum)" "gradient:variability(max)+variability=0&time(max)+variability=0.5&time(max)+variability=1&time(max)"``  
  ``./batch_tester.sh plot study "pFollows,collection:time(num)"`` 
  ``pypy data/threshold.py > data/threshold.v0.txt``  
  Results will be written in the `data/` directory into text files containing Asymptote source code able to produce the corresponding plots.

Different plots can be produced by varying the final string in the above commands. For a brief explanation of the plot descriptive syntax, issue the following command:

``./plot_builder.py``

* Lastly, Asymptote files can be compiled in PDF either manually or issuing the following command from the `data` subfolder:  
  ``../batch_tester.sh asy *.v*.txt``


### Inspecting the code and simulation GUI

Open Eclipse, click on "File > Import" and then on "Gradle > Existing Gradle Project", then select the folder of the repository donwloaded in the previous step.

To properly visualize the source files you should also install a Yaml editor (as `YEdit` in the Eclipse Marketplace) and the Protelis Parser through "Help > Install New Software" with the following address:

``http://efesto.apice.unibo.it/protelis-build/protelis-parser/protelis.parser.repository/target/repository/``

The files with the source code are in `src/main/protelis/` and contain the following:

* `movement.pt`: utility functions on geometry and device movement
* `utils.pt`: general utility functions
* `G.pt`: code of all distance estimation algorithms
* `C.pt`: code of all collection algorithms
* `S.pt`: code of Voronoi partitioning algorithms
* `isolation.pt`: code of the isolation tests (as in Sections IV-A and IV-B)
* `study.pt`: code of the case study (as in Section IV-C)

The files describing the environment are in `src/main/yaml/` and contain the following:

* `isolation.yaml`: environment used for the isolation tests (as in Sections IV-A and IV-B)
* `study.yaml`: environment used for the case study (as in Section IV-C)

In the `java` folder, there are files improving on the current Alchemist version. In the `resources` folder, there are effect files for the GUI presentation, together with raw data on the GPS traces used in the case study:

* `isolation.aes`: effects used for the isolation tests (as in Sections IV-A and IV-B)
* `study.aes`: effects used for the case study (as in Section IV-C)

In order to run the simulations with a GUI, create a Run Configuration in Eclipse with the following settings.

* Main class: `it.unibo.alchemist.Alchemist`
* Program arguments: `-g src/main/resources/X.aes -y src/main/yaml/X.yml` where `X` can be either `isolation` or `study`
