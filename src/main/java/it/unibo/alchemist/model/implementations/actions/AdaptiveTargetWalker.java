/**
 * 
 */
package it.unibo.alchemist.model.implementations.actions;

import it.unibo.alchemist.model.implementations.molecules.SimpleMolecule;
import it.unibo.alchemist.model.implementations.movestrategies.routing.OnStreets;
import it.unibo.alchemist.model.implementations.movestrategies.target.FollowTargetOnMap;
import it.unibo.alchemist.model.implementations.positions.LatLongPosition;
import it.unibo.alchemist.model.interfaces.MapEnvironment;
import it.unibo.alchemist.model.interfaces.Molecule;
import it.unibo.alchemist.model.interfaces.Node;
import it.unibo.alchemist.model.interfaces.Position;
import it.unibo.alchemist.model.interfaces.Reaction;
import it.unibo.alchemist.model.interfaces.Vehicle;
import it.unibo.alchemist.model.interfaces.movestrategies.SpeedSelectionStrategy;

/**
 *
 * @param <T>
 */
public class AdaptiveTargetWalker<T> extends MoveOnMap<T> {

    private static final long serialVersionUID = 5097382908560832035L;
    
    public static class MoleculeSpeed<T> implements SpeedSelectionStrategy<T> {
        private static final long serialVersionUID = 1746429998480123049L;
        private final double bs;
        private final Molecule sp;
        private final Node<T> n;
        private final double r;

        /**
         * @param reaction
         *            the reaction
         * @param speed
         *            the speed, in meters/second
         */
        public MoleculeSpeed(final Reaction<T> reaction, final Node<T> node, final Molecule speed, final double baseSpeed) {
            r = reaction.getRate();
            n = node;
            sp = speed;
            bs = baseSpeed;
        }

        @Override
        public double getCurrentSpeed(final Position target) {
            Object cs = n.getConcentration(sp);
            return (cs == null ? bs : (Double)cs) / r;
        }

    }

    /**
     * @param environment
     *            the environment
     * @param node
     *            the node
     * @param reaction
     *            the reaction. Will be used to compute the distance to walk in
     *            every step, relying on {@link Reaction}'s getRate() method.
     * @param trackMolecule
     *            the molecule to track. Its value will be read when it is time
     *            to compute a new target. If it is a {@link LatLongPosition},
     *            it will be used as-is. If it is an {@link Iterable}, the first
     *            two values (if they are present and they are numbers, or
     *            Strings parse-able to numbers) will be used to create a new
     *            {@link LatLongPosition}. Otherwise, the {@link Object} bound
     *            to this {@link Molecule} will be converted to a String, and
     *            the String will be parsed using the float regular expression
     *            matcher in Javalib.
     * @param speedMolecule
     *            the molecule that decides the speed at which this {@link MoveOnMap} will move
     * @param speed
     *            the default speed
     */
    public AdaptiveTargetWalker(
            final MapEnvironment<T> environment,
            final Node<T> node,
            final Reaction<T> reaction,
            final Molecule trackMolecule,
            final Molecule speedMolecule,
            final double speed) {
        super(environment, node,
                new OnStreets<>(environment, Vehicle.FOOT),
                new MoleculeSpeed<>(reaction, node, speedMolecule, speed),
                new FollowTargetOnMap<>(environment, node, trackMolecule));
    }

    /**
     * @param environment
     *            the environment
     * @param node
     *            the node
     * @param reaction
     *            the reaction. Will be used to compute the distance to walk in
     *            every step, relying on {@link Reaction}'s getRate() method.
     * @param trackMolecule
     *            the molecule to track. Its value will be read when it is time
     *            to compute a new target. If it is a {@link LatLongPosition},
     *            it will be used as-is. If it is an {@link Iterable}, the first
     *            two values (if they are present and they are numbers, or
     *            Strings parse-able to numbers) will be used to create a new
     *            {@link LatLongPosition}. Otherwise, the {@link Object} bound
     *            to this {@link Molecule} will be converted to a String, and
     *            the String will be parsed using the float regular expression
     *            matcher in Javalib.
     * @param speedMolecule
     *            the molecule that decides the speed at which this {@link MoveOnMap} will move
     */
    public AdaptiveTargetWalker(
            final MapEnvironment<T> environment,
            final Node<T> node,
            final Reaction<T> reaction,
            final String trackMolecule,
            final String speedMolecule,
            final double speed) {
        this(environment, node, reaction, new SimpleMolecule(trackMolecule), new SimpleMolecule(speedMolecule), speed);
    }
}
