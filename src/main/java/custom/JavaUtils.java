package custom;

public class JavaUtils {
	public static Object debug(Object x) {
		return debug(x, "DEBUG");
	}
	
	public static Object debug(Object x, String s) {
		System.out.println(s + ": " + x);
		return x;
	}
}
